Step 1:
Locate Division and if divide by 0 raise error.

Step 2:
If divide by variable, stop recursive and analyse value of the variable

Step 3:
If divide by function, stop recursive and analyse return value of this function.
