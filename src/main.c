/*
 * Copyright (c) 2015, Quentin Schwerkolt
 * All rights reserved.
 *
 */

#if defined(HAVE_CONFIG_H)
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#include <clang-c/Index.h>
#include <clang-c/CXString.h>

#include "adbg.h"

static struct {
	int	cursorKing;
	int	(*callback)(CXCursor, CXCursor, CXClientData);
} cursors[] = {
	{ CXCursor_DeclRefExpr,		declRefExpr },
	{ CXCursor_BinaryOperator,	binaryOperator },
	{ CXCursor_FunctionDecl,	functionDecl },
	{ CXCursor_IntegerLiteral,	integerLiteral },
	{ -1,				defaultAction }
};

static CXTranslationUnit tu;

enum CXChildVisitResult
visitor(CXCursor current, CXCursor parent, CXClientData data)
{
	int i;
	enum CXCursorKind kind;

	kind = clang_getCursorKind(current);
	for (i = 0; cursors[i].cursorKing != -1; ++i) {
		if (cursors[i].cursorKing == kind)
			break;
	}
	cursors[i].callback(current, parent, data);
	return (CXChildVisit_Continue);
}

int
main(int argc __attribute__((unused)), char **argv)
{
	CXIndex index;
	const char *filename;

	filename = argv[1];
	if (filename == NULL)
		return (2);
	index = clang_createIndex(0, 0);
	tu = clang_createTranslationUnitFromSourceFile(index, filename, 0, NULL,
						       0, 0);

	clang_visitChildren(clang_getTranslationUnitCursor(tu), visitor, 0);

	clang_disposeTranslationUnit(tu);
	clang_disposeIndex(index);
	return (0);
}
