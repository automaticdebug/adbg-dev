Une equation est représentée par une structure contenant deux node ainsi qu'un operateur.
Le type utilisateur t_equa est le type ayant le plus haut niveau d'abstraction.


//////////////////////////////
// struct          s_equa
// {
//   t_OP          operator;
//   t_node        *first;
//   t_node        *sec;
// };

Chaque node représente une equation ou une valeur.
Les fictions d'opération manipulent des nodes et retournent des nodes.

Dans l'exemple (4*3)+1

operator = ADD ('+')
first 	 = 4*3 (equation)
sec 	 = 1   (node-value)

////////////////////////////
// struct          s_node
// {
//   enum e_Node   nodeType;
//   void          *data;
//   t_node        *prev;
//   t_node        *next;
// };

Le sous type des nodes, qu'il soit 'valeur' ou 'interval' n'est utilisé que dans les fonctions d'operations.
A l'avenir un node pourra aussi etre de type clang 'CXCursor'.

Un node peut aussi etre chainé a un autre node, ce qui représente une Union entre deux ensemble

Dans l'exemple [-10;-7] U [3;12]

*prev = NULL
nodeType = VALUE
*data = [-10;-7] (Range)
*next = ----|
            |
*prev = ----|
nodeType = VALUE
*data = [3;12] (Range)
*next = NULL

L'utilisateur de cette fonction de résolution devra construire son arbre d'equation correctement au préalable.
Il pourra utiliser les fonction cre_* ou encore *_equa servant a construire plus rapidement l'arbre d'equation.

Le resultat de l'equation sera retourné sous la forme d'une node contenant le resultat. Comme expliqué plus haut, ce node pourra etre chainé.
