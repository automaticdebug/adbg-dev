#ifndef __ADBG_EQUATION_H__
#define __ADBG_EQUATION_H__

#include "node.h"

typedef struct s_equa t_equa;

typedef enum e_Op
  {
    ADD = 0,
    SUB,
    MULT,
    MOD,
    DIV
  }	t_OP;

struct		s_equa
{
  t_OP		operator;
  t_node	*first;
  t_node	*sec;
};

int		resolv(t_equa *, t_node **);

#endif /* __ADBG_EQUATION_H__ */
