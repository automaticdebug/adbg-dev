#ifndef __ADBG_OPERATION_H__
#define __ADBG_OPERATION_H__

typedef t_node* (*op_func)(t_node *, t_node *);

t_node		*opp_add(t_node *, t_node *);
t_node		*opp_sub(t_node *, t_node *);
t_node		*opp_mul(t_node *, t_node *);
t_node		*opp_mod(t_node *, t_node *);
t_node		*opp_div(t_node *, t_node *);

#endif /* __ADBG_OPERATION_H__ */
