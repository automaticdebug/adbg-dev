#include <stdlib.h>

#include <stdio.h>

#include "equation.h"
#include "range.h"
int		resolv(t_equa *, t_node **);

t_value		cre_value(int i, Sym s)
{
  t_value val;
  val.nbr = i;
  val.sym = s;
  return (val);
}

t_range		*cre_range(t_value v1, t_value v2)
{
  t_range	*r;
  if ((r = malloc(sizeof(t_range))) == NULL)
    exit(-21);
  r->v1 = v1;
  r->v2 = v2;
  return (r);
}

t_node		*cre_node(typeNode t, void *data)
{
  t_node *n;
  if ((n = malloc(sizeof(t_node))) == NULL)
    exit(-21);
  n->nodeType = t;
  n->data = data;
  n->prev = NULL;
  n->next = NULL;
  return (n);
}

t_node		*union_node(t_node *n1, t_node *n2)
{
  n1->next = n2;
  n2->prev = n1;
  return (n1);
}

t_equa		*add_equa(t_node *n1, t_node *n2)
{
  t_equa *e;
  if ((e = malloc(sizeof(t_equa))) == NULL)
    exit(-21);
  // E1 = [-10 ; -7] U [3 ; 12] + [-4 ; 1] U [5 ; 7]
  e->operator = ADD;
  e->first = n1;
  e->sec = n2;
  return (e);
}

int main()
{
  // n1 = [-10;-7]U[3;12]
  t_node *n1 = union_node(cre_node(VALUE, (void*)cre_range(cre_value(-10, INCLUDE),
							   cre_value(-7, INCLUDE))),
			  cre_node(VALUE, (void*)cre_range(cre_value(3, INCLUDE),
							   cre_value(12, INCLUDE))));
  // n2 = [-4;1]U[5;7]
  t_node *n2 = union_node(cre_node(VALUE, (void*)cre_range(cre_value(-4, INCLUDE),
							   cre_value(1, INCLUDE))),
			  cre_node(VALUE, (void*)cre_range(cre_value(5, INCLUDE),
							   cre_value(7, INCLUDE))));
  
  t_node *res;
  printf("E1 = [-10 ; -7] U [3 ; 12] + [-4 ; 1] U [5 ; 7]\nE1 =");

  resolv(add_equa(n1, n2), &res);


  do {
    printf(" [%d ; %d]", ((t_range*)res->data)->v1.nbr, ((t_range*)res->data)->v2.nbr);
  } while (res->next != NULL && (res = res->next) && (printf(" U")));
  printf("\n");

  get_first_node(&res);
  release_node(res);

  return (0);
}
