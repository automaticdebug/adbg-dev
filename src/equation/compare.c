#include <stdlib.h>

#include "node.h"
#include "range.h"

int		is_in_range(t_value *v, t_range *r)
{
  if (v->nbr > r->v1.nbr && v->nbr < r->v2.nbr)
    {
      if (v->sym != NO && (v = v + 1))
	{
	  if (v->nbr < r->v1.nbr || v->nbr > r->v2.nbr)
	    return (1);
	  return (is_in_range(v, r));
	}
      else
	return (1);
    }
  return (0);
}

void		*merge_range(t_value *v, t_range *r)
{
  t_value	*tmp;
  if (v->sym != NO)
    {
      tmp = v + 1;  
      r->v1.nbr = (v->nbr < r->v1.nbr) ? v->nbr : r->v1.nbr;
      r->v2.nbr = (tmp->nbr > r->v2.nbr) ? tmp->nbr : r->v2.nbr;
    }
  return ((void*)r);
}

t_node		*merge_node(t_node *tmp, t_node **n)
{
  (*n)->data = tmp->data;
  (*n)->next = tmp->next;
  if (tmp->next != NULL)
    tmp->next->prev = tmp->prev;
  free(tmp);
  return (*n);
}

t_node		*glob_merge(t_value *v1, t_value *v2, t_node *tmp, t_node **n)
{
  if (v2->sym != NO && is_in_range(v1, (t_range*)v2) != 0)
    {
      tmp->data = merge_range(v1, (t_range*)v2);
      free(v1);
      return (merge_node(tmp, n));
    }
  if (v1->sym != NO && is_in_range(v2, (t_range*)v1) != 0)
    {
      tmp->data = merge_range(v2, (t_range*)v1);
      free(v2);
      return (merge_node(tmp, n));
    }
  return (tmp->next);
}

t_node		*compare_value(t_node *tmp, t_node **n)
{

  if ((tmp != (*n)) &&							\
      (((t_value*)tmp->data)->sym != NO && ((t_value*)(*n)->data)->sym != NO))
    return (glob_merge(((t_value*)tmp->data), ((t_value*)(*n)->data), tmp, n));
  return (tmp->next);
}
