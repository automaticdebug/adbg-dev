/*
 * Copyright (c) 2015, Quentin Schwerkolt
 * All rights reserved.
 *
 *
 */

#ifndef __ADBG_H
#define __ADBG_H

enum CXChildVisitResult	visitor(CXCursor current, CXCursor parent, CXClientData data);
int	integerLiteral(CXCursor current, CXCursor parent, CXClientData data);
int     binaryOperator(CXCursor current, CXCursor parent, CXClientData data);
int     declRefExpr(CXCursor current, CXCursor parent, CXClientData data);
int     defaultAction(CXCursor current, CXCursor parent, CXClientData data);
int     functionDecl(CXCursor current, CXCursor parent, CXClientData data);

#endif	/* __ADBG_H */
