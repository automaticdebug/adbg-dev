/*
 * Copyright (c) 2015, Quentin Schwerkolt
 * All rights reserved.
 *
 *
 */

#if defined(HAVE_CONFIG_H)
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#include <clang-c/Index.h>
#include <clang-c/CXString.h>

#include "string.h"

#include "adbg.h"

int
binaryOperator(CXCursor current, CXCursor parent __attribute__((unused)), CXClientData data __attribute__((unused)))
{
  int rec = 1;
  CXString str;
  CXToken *tokens;
  CXSourceRange range;
  unsigned ntokens;
  CXTranslationUnit tu;

  tu = clang_Cursor_getTranslationUnit(current);
  range = clang_getCursorExtent(current);
  clang_tokenize(tu, range, &tokens, &ntokens);

  for (unsigned i = 0; i < ntokens; i++) {
    str = clang_getTokenSpelling(tu, tokens[i]);
  const char *tmp = clang_getCString(str);
    if (strcmp("/", tmp) == 0)
      rec = 0;
    //      printf("Binary operator: %s - %u/%u\n", tmp, i, ntokens);
    clang_disposeString(str);
  }
  printf("\n");
  
  clang_disposeTokens(tu, tokens, ntokens);
  
  printf("Je verifie mon signe \n");
  if (rec == 1)
    clang_visitChildren(current, visitor, 0);
  else
    printf("Il y a un symbol de division\n");
  return (0);
}
