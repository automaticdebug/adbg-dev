/*
 * Copyright (c) 2015, Quentin Schwerkolt
 * All rights reserved.
 *
 */

#if defined(HAVE_CONFIG_H)
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>

#include <clang-c/Index.h>
#include <clang-c/CXString.h>

#include "adbg.h"

int
defaultAction(CXCursor current, CXCursor parent __attribute__((unused)), CXClientData data __attribute__((unused)))
{
	clang_visitChildren(current, visitor, 0);
	return (0);
}
