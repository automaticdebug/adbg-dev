
#include <stdlib.h>
#include <stdio.h>

#include <clang-c/Index.h>
#include <clang-c/CXString.h>

#include "adbg.h"
#include "customast.h"

CustomNode	*g_motherofall = NULL;
CustomNode	*g_current = NULL;

CustomNode *getMotherOfAll(void)
{
	return g_motherofall;
}

void setMotherOfAll(CustomNode *node)
{
	g_motherofall = node;
}

CustomNode *getCurrent(void)
{
	return g_current;
}

void setCurrent(CustomNode *node)
{
	g_current = node;
}

CustomNode *makeNode(CustomNode *parent, int kind)
{
	CustomNode *node = malloc(sizeof(*node));

	if (node == NULL)
	{
		return NULL;
	}
	node->parent = parent;
	node->next = NULL;
	node->lhs = NULL;
	node->rhs = NULL;
	node->kind = kind;

	return node;
}

void printIndent(int n)
{
	int i;

	for (i = 0; i < n; i++)
	{
		printf(" ");
	}
}

void printCustomAst(CustomNode *node, int indent)
{
	printIndent(indent);
	printf("[%d] {\n", node->kind);
	if (node->lhs != NULL)
	{
		printIndent(indent+3);
		printf("LHS {\n");
		printCustomAst(node->lhs, indent+6);
		printIndent(indent+3);
		printf("}\n");
	}
	if (node->rhs != NULL)
	{
		printIndent(indent+3);
		printf("RHS {\n");
		printCustomAst(node->lhs, indent+6);
		printIndent(indent+3);
		printf("}\n");
	}
	printIndent(indent);
	printf("}\n");
	if (node->next != NULL)
	{
		printCustomAst(node->next, indent);
	}
}

void appendNode(CustomNode *node, CustomNode **collection)
{
	CustomNode *current = *collection;

	if (*collection == NULL)
	{
		*collection = node;
		return;
	}
	while (current->next != NULL)
	{
		current = current->next;
	}
	current->next = node;
}

