/*
 * Copyright (c) 2015, Quentin Schwerkolt
 * All rights reserved.
 *
 */

#if defined(HAVE_CONFIG_H)
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>

#include <clang-c/Index.h>
#include <clang-c/CXString.h>

#include "adbg.h"

int
functionDecl(CXCursor current, CXCursor parent __attribute__((unused)), CXClientData data __attribute__((unused)))
{
	CXString s;

	s = clang_getCursorSpelling(current);
	printf("Je rentre dans la FunctionDecl: `%s'\n", clang_getCString(s));
	clang_disposeString(s);
	clang_visitChildren(current, visitor, 0);
	s = clang_getCursorSpelling(current);
	printf("Je sors de la FunctionDecl: `%s'\n", clang_getCString(s));
	clang_disposeString(s);
	return (0);
}
