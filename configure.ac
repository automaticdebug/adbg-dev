dnl
dnl Copyright (c) 2015, Quentin Schwerkolt
dnl All rights reserved.
dnl
dnl
dnl

AC_INIT([adbg], [0.0.0], [automaticdebugger_2016@labeip.epitech.eu])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([m4])
AM_INIT_AUTOMAKE([subdir-objects foreign -Wall -Werror])

clanglibs="-lclang"
clanginc=
have_clang=no

AC_ARG_WITH([clang],
[AS_HELP_STRING([--with-clang=PATH],
		[specify prefix directory for installed Clang package.
		 Equivalent to --with-clang-include=PATH/include
		 plus --with-clang-lib=PATH/lib])])
AC_ARG_WITH([clang-include],
[AS_HELP_STRING([--with-clang-include=PATH],
		[specify directory for installed Clang include files])])
AC_ARG_WITH([clang-lib],
[AS_HELP_STRING([--with-clang-lib=PATH],
		[specify directory for the installed Clang library])])

if test "x$with_clang" != x; then
   clanglibs="-L$with_clang/lib $clanglibs"
   clanginc="-I$with_clang/include $clanginc"
fi
if test "x$with_clang_include" != x; then
   clanginc="-I$with_clang_include $clanginc"
fi
if test "x$with_clang_lib" != x; then
   clanglibs="-L$with_clang_lib $clanglibs"
fi

AC_PROG_CC
AC_PROG_CC_C99
AC_PROG_CC_C_O
AM_PROG_CC_C_O

AC_PROG_INSTALL

AC_USE_SYSTEM_EXTENSIONS

AC_HEADER_STDC(,, AC_MSG_ERROR([ansi headers are required.]))
AC_CHECK_HEADERS([stdio.h],, AC_MSG_ERROR([<stdio.h> is required.]))

AC_CHECK_FUNCS([printf],, AC_MSG_ERROR([prinnf(3) is required.]))
AC_CHECK_FUNCS([getprogname],, AC_MSG_WARN([getprogname(3) is missing.]))
AC_CHECK_FUNCS([strlcpy],, AC_MSG_WARN([strlcpy(3) is missing.]))

if test "x$have_clang" = xno; then
   have_clang=yes
   saved_CFLAGS="$CFLAGS"
   CFLAGS="$CFLAGS $clanginc"
   # Check for an acceptable version of clang-c/Index.h.
   AC_MSG_CHECKING([for the correct version for clang-c/Index.h])
   AC_TRY_COMPILE([#include <clang-c/Index.h>], [
   #if CINDEX_VERSION < CINDEX_VERSION_ENCODE(0,20)
   choke me
   #endif
   ], [AC_MSG_RESULT([yes])], [AC_MSG_RESULT([buggy but acceptable])],
   [AC_MSG_RESULT([no]); have_clang=no])

   # Now check for the Clang library.
   if test x"$have_clang" = xyes; then
      saved_LIBS="$LIBS"
      LIBS="$LIBS $clanglibs"
      AC_MSG_CHECKING([for the correct version of the clang library])
      AC_TRY_LINK([#include <clang-c/Index.h>], [
      CXIndex i;
      i = clang_createIndex (0, 0);
      ], [AC_MSG_RESULT([yes])], [AC_MSG_RESULT([no]); have_clang=no])
      LIBS="$saved_LIBS"
   fi

   CFLAGS="$saved_CFLAGS"
   if test x$have_clang != xyes; then
      AC_MSG_ERROR([Clang 3.4+ is required.])
   fi
fi

AC_SUBST(clanglibs)
AC_SUBST(clanginc)

AC_CONFIG_HEADERS([config.h])
AC_CONFIG_FILES([Makefile src/Makefile src/customast/Makefile src/restrack/Makefile])
AC_OUTPUT
